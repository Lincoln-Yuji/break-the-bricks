extends StaticBody2D

var durability: int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group(Groups.BRICK)
	$Label.text = durability as String
	set_right_color()

func decrement_life():
	durability -= 1
	if durability == 0:
		queue_free()
	$Label.text = durability as String
	set_right_color()

func set_right_color():
	for i in range(5):
		if durability <= GameProperties.brick_delimiters[i]:
			$Sprite.modulate = Color(GameProperties.brick_colors[i])
			return
	$Sprite.modulate = Color(GameProperties.brick_colors[5])

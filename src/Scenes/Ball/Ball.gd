extends KinematicBody2D

const SPEED: float = 600.0

var direction: Vector2 = Vector2.ZERO

enum {NORMAL, RETURNING}
var current_state

signal destroyed

# Called when the node enters the scene tree for the first time.
func _ready():
	current_state = NORMAL

func _physics_process(delta):
	match current_state:
		NORMAL:
			var collision: KinematicCollision2D = move_and_collide(direction * SPEED * delta)
			if collision:
				direction = direction.bounce(collision.normal)
				var collider = collision.collider
				if collider.is_in_group(Groups.BRICK):
					collider.decrement_life()
				elif collider.is_in_group(Groups.FLOOR):
					destroy()
		RETURNING:
			var player_pos: Vector2 = get_tree().get_nodes_in_group(Groups.PLAYER)[0].position
			direction = (player_pos - self.position).normalized()
			var movement: Vector2 = direction * SPEED * delta
			if (player_pos - self.position).length_squared() < movement.length_squared():
				queue_free()
			else:
				translate(movement)
			
func destroy():
	emit_signal("destroyed", self.position)
	queue_free()

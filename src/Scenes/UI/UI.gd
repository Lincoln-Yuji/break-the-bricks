extends Control

onready var pause_button = $PauseButton
onready var force_down_button = $ForceDownButton

# Called when the node enters the scene tree for the first time.
func _ready():
	pause_button.connect("button_down", self, "_on_PauseButton_pressed")

func set_tries(amount: int):
	$Tries.text = amount as String

func _on_PauseButton_pressed():
	get_tree().paused = !get_tree().paused
	
func _on_Player_out_of_balls():
	force_down_button.visible = true
	
func _on_Player_shoot_finished():
	force_down_button.visible = false

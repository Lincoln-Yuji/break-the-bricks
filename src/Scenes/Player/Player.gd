extends Node2D

onready var BALL_SCENE: PackedScene = preload("res://Scenes/Ball/Ball.tscn")

onready var ray_cast: RayCast2D = $RayCast2D
onready var line: Line2D = $Line2D

var amount_of_balls: int = 80
var ball_container: Node2D

var is_placed: bool = false

var ray_cast_max_length: float = 1000.0

enum {AIMING, SHOOTING, STANDBY}
var current_state

signal shot_finished
signal out_of_balls

func _ready():
	add_to_group(Groups.PLAYER)
	change_state(STANDBY)

func _process(_delta):
	match current_state:
		STANDBY:
			if Input.is_action_just_pressed("left_mouse_button") and get_viewport().get_mouse_position().y > 110:
				change_state(AIMING)
		AIMING:
			var mouse_position = get_viewport().get_mouse_position()
			adjust_laser(mouse_position)
			if Input.is_action_just_released("left_mouse_button"):
				shoot((mouse_position - self.position).normalized())
				change_state(SHOOTING)
		SHOOTING:
			if ball_container.get_child_count() == 0:
				emit_signal("shot_finished")
				change_state(STANDBY)
		
func shoot(shoot_direction):
	var shoot_pos = self.position
	for _i in range(amount_of_balls):
		var ball = BALL_SCENE.instance()
		ball.position = shoot_pos
		ball.direction = shoot_direction
		ball.connect("destroyed", self, "_on_Ball_destroyed")
		ball_container.add_child(ball)
		yield(get_tree().create_timer(0.1), "timeout")
	emit_signal("out_of_balls")

func _on_Ball_destroyed(ball_position):
	if !is_placed:
		self.position.x = ball_position.x
		is_placed = true

func change_state(state):
	match state:
		STANDBY:
			ray_cast.enabled = false
			line.visible = false
		AIMING:
			ray_cast.enabled = true
			line.visible = true
		SHOOTING:
			ray_cast.enabled = false
			line.visible = false
			is_placed = false
	current_state = state

func adjust_laser(mouse_position):
	var aim_direction = (mouse_position - self.position).normalized()
	ray_cast.cast_to = aim_direction * ray_cast_max_length
	if ray_cast.is_colliding():
		line.points[1] = ray_cast.get_collision_point() - self.position
	else:
		line.points[1] = ray_cast.cast_to

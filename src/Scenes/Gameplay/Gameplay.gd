extends Node2D

const TAB_W: int = 9
const TAB_H: int = 6

const MAX_LEVEL: int = 3

onready var BRICK_SCENE: PackedScene = preload("res://Scenes/Brick/Brick.tscn")

onready var player = $Player
onready var ui = $CanvasLayer/UI
onready var bricks = $Bricks
onready var congrats_message = $CanvasLayer/Congrats
onready var game_over = $CanvasLayer/GameOver

onready var tries_amount: int = 5 + 5 * GameProperties.level_to_load

# Called when the node enters the scene tree for the first time.
func _ready():
	player.connect("shot_finished", self, "_on_Player_shot_finished")
	player.ball_container = $BallContainer
	
	player.connect("shot_finished", ui, "_on_Player_shoot_finished")
	player.connect("out_of_balls", ui, "_on_Player_out_of_balls")
	ui.force_down_button.connect("button_down", self, "_on_ForceDownButton_pressed")
	ui.set_tries(tries_amount)
	
	configure_bricks(GameProperties.level_to_load)

func _on_Player_shot_finished():
	tries_amount -= 1
	ui.set_tries(tries_amount)

func _on_ForceDownButton_pressed():
	for ball in $BallContainer.get_children():
		ball.current_state = ball.RETURNING

func configure_bricks(level: int):
	var file = File.new()
	file.open("res://Levels/Level0%d.txt" % level, File.READ)
	for i in range(TAB_H):
		var line: String = file.get_line()
		for j in range(TAB_W):
			if line[j] != '_':
				spawn_brick(i, j, line[j])
	file.close()

func spawn_brick(i: int, j: int, durability_token: String):
	var brick = BRICK_SCENE.instance()
	brick.position = Vector2(get_viewport_rect().size.x / 2 - 250 + j * 62.5, 200 + i * 62.5)
	brick.durability = GameProperties.BRICK_DURABILITIES[durability_token]
	bricks.add_child(brick)

func _process(delta):
	if bricks.get_child_count() == 0:
		player.set_process(false)
		congrats_message.visible = true
		yield(get_tree().create_timer(2.0), "timeout")
		if GameProperties.level_to_load < MAX_LEVEL:
			GameProperties.level_to_load += 1
			get_tree().change_scene(GameProperties.GAMEPLAY_PATH)
		else:
			get_tree().change_scene(GameProperties.MENU_PATH)
	if tries_amount == 0 and player.current_state == player.STANDBY:
		player.set_process(false)
		game_over.visible = true
		yield(get_tree().create_timer(2.0), "timeout")
		get_tree().change_scene(GameProperties.MENU_PATH)
		

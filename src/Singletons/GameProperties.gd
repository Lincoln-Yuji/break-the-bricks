extends Node

const BRICK_DURABILITIES = {
	'o': 5,
	'x': 20,
	'w': 40,
	'#': 100,
	'@': 180
}

const brick_delimiters: Array = [5, 20, 40, 100, 180]
const brick_colors: Array = ["ffffff", "ffa45e", "ee4444", "ec4175", "9a77cf", "693884"]

const GAMEPLAY_PATH: String = "res://Scenes/Gameplay/Gameplay.tscn"
const MENU_PATH: String = "res://Scenes/Menu/Menu.tscn"

var level_to_load: int = 1
